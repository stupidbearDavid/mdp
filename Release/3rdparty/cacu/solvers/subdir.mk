################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../3rdparty/cacu/solvers/adam_solver.cc \
../3rdparty/cacu/solvers/rmsprop_solver.cc \
../3rdparty/cacu/solvers/sgd_solver.cc \
../3rdparty/cacu/solvers/solver_base.cc 

CC_DEPS += \
./3rdparty/cacu/solvers/adam_solver.d \
./3rdparty/cacu/solvers/rmsprop_solver.d \
./3rdparty/cacu/solvers/sgd_solver.d \
./3rdparty/cacu/solvers/solver_base.d 

OBJS += \
./3rdparty/cacu/solvers/adam_solver.o \
./3rdparty/cacu/solvers/rmsprop_solver.o \
./3rdparty/cacu/solvers/sgd_solver.o \
./3rdparty/cacu/solvers/solver_base.o 


# Each subdirectory must supply rules for building sources it contributes
3rdparty/cacu/solvers/%.o: ../3rdparty/cacu/solvers/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -std=c++0x -I/Users/seallhf/Documents/openblas/include -I/Users/seallhf/Documents/opencv/include -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


