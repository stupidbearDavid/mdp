################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../framework/action_base.cc \
../framework/po_tuple.cc \
../framework/reward.cc \
../framework/state_base.cc \
../framework/transfer_prob.cc \
../framework/tuple.cc 

CC_DEPS += \
./framework/action_base.d \
./framework/po_tuple.d \
./framework/reward.d \
./framework/state_base.d \
./framework/transfer_prob.d \
./framework/tuple.d 

OBJS += \
./framework/action_base.o \
./framework/po_tuple.o \
./framework/reward.o \
./framework/state_base.o \
./framework/transfer_prob.o \
./framework/tuple.o 


# Each subdirectory must supply rules for building sources it contributes
framework/%.o: ../framework/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -std=c++0x -I/Users/seallhf/Documents/openblas/include -I/Users/seallhf/Documents/opencv/include -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


